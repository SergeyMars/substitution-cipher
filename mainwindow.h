#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void clear_fields();
    void make_substitution();
    void only_english(QString text, int punctuation);
    void only_russian(QString text, int punctuation);

    char get_random_char(int symbolWarning);
    int get_random_int(int limit);
    int getposition(const char *array, size_t size, char c);
    int getposition_ru(const char * array, const char * c);
    bool check_if_punctuation(char letter);
    bool check_if_one_byte_letter(char letter);
    void shuffle_array(char * str_shuffled, int lang);

    void on_clearButton_clicked();
    void on_pushButton_2_clicked();
    void on_checkBox_en_stateChanged(int arg1);
    void on_checkBox_ru_stateChanged(int arg1);
    void on_checkBox_punc_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
