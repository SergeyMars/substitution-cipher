#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <iostream>
#include <cstring>
#include <cstdio>
#include <random>

using namespace std;

#include <QDebug>

QString qstrTable;
int onlyEn, onlyRu, doNotTouchPunctuation = 0;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clear_fields()
{
    ui->originTextEditor->setPlainText("");
    ui->substitutionedTextEditor->setPlainText("");
}

int MainWindow::getposition(const char *array, size_t size, char c)
{
//     const char* end = array + size;
//     const char* match = std::find(array, end, c);
//     return (end == match)? -1 : (match-array);

     string main_str(array);
     return main_str.find(c);
}

int MainWindow::getposition_ru(const char * array, const char * c)
{
    string main_str(array);
    string sub_str(c);
    return main_str.find(sub_str);
}

char MainWindow::get_random_char(int symbolWarning)
{
    char letter;
    int r;

    if (symbolWarning>0) { // оставляем знаки, меняем только en
        random_device dev;
        mt19937 rng(dev());
        uniform_int_distribution<std::mt19937::result_type> dist6(0,25);

        r = dist6(rng);
        if (r>12) {
            letter = 'A' + r;
        }
        else {
            letter = 'a' + r;
        }

    }
    else {
        random_device dev;
        mt19937 rng(dev());
        uniform_int_distribution<std::mt19937::result_type> dist6(0,82);

        r = dist6(rng);
        letter = ' ' + r;
    }

    return letter;    // Convert to a character from a-z
}

int MainWindow::get_random_int(int limit)
{
    int r;
    random_device dev;
    mt19937 rng(dev());
    uniform_int_distribution<std::mt19937::result_type> dist6(0,limit);
    r = dist6(rng);

    return r;
}

bool MainWindow::check_if_punctuation(char letter)
{
    if (((' ' <= letter) && (letter < '0')) || (('[' <= letter) && (letter < 'a')) || ((':' <= letter) && (letter < 'A')) || (letter == '\n') || (letter == '\r'))
    {
        return true;
    }
    return false;
}

bool MainWindow::check_if_one_byte_letter(char letter)
{
    if ( ((' ' <= letter) && (letter < '~')) || (letter == '\n') || (letter == '\r'))
    {
        return true;
    }
    return false;
}

void MainWindow::only_english(QString text, int punctuation)
{
    string originText = text.toStdString();
    int originTextLen = originText.length();
    char encryptedText[originTextLen];

    char strUniq [originTextLen];
    memset(strUniq, '\0', originTextLen);

    // Формируем строку из всех уникальных символов в тексте
    int i=0, j=0;
    for (i; i<originTextLen; i++)
    {
        if (i == 0)
        {
            strUniq[j] = originText[i];
            j++;
        }
        else
        {
            bool compareFlag = false;

            for (int j_inner(0); j_inner < j; j_inner++)
            {
                if (originText[i] == strUniq[j_inner])
                {
                    compareFlag = true;
                    j_inner = j;
                }
            }
            if (!compareFlag)
            {
                strUniq[j] = originText[i];
                j++;
            }
        }
    }
//     cout << "Uniq: " << strUniq << endl; // debug
    int strUniqLen = strlen(strUniq); // без 0 символа
    char strSubst[strUniqLen+1]; // для нулевого символа, чтобы вывод нормально отслеживать через print
    memset(strSubst, '\0', strUniqLen+1);

    if (punctuation > 0)
    {
        char latin[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char latin_shuffled[strlen(latin)];
        shuffle_array(latin_shuffled, 0);

/*        char latin_with_punctuation[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -:;`'\"!@#$%^&*,.?/()[\\]{|}~";
        char latin_with_punctuation_shuffled[strlen(latin_with_punctuation)];
        shuffle_array(latin_with_punctuation_shuffled, 1);*/

        int i = 0;
        for (i; i<originTextLen; )
        {
            if (check_if_punctuation(originText[i])) {
                encryptedText[i] = originText[i];
            }
            else {
                int position = getposition(latin, strlen(latin), originText[i]);
                encryptedText[i] = *(latin_shuffled+position);
            }
            i++;
        }

        ui->substitutionedTextEditor->setPlainText( QString(encryptedText) );
        ui->substitutionTableEditor->setPlainText( QString(latin) );
        ui->substitutionTableEditor->setPlainText(QString(latin_shuffled) );
    }
    else {
        i = 0; j = 0;
        for (i; i<strUniqLen; i++)
        {
            if (i == 0)
            {
                strSubst[i] = get_random_char(punctuation);
                j++;
            }
            else
            {
                bool compareFlag;
                do {
                    compareFlag = false;

                    strSubst[i] = get_random_char(punctuation);

                    for (int i_inner(0); i_inner < j; i_inner++)
                    {
                        if (strSubst[i] == strSubst[i_inner])
                        {
                            compareFlag = true;
                            i_inner = j; // переходим в конец
                        }
                    }
                } while(compareFlag);
                j++;
            }
        }
        // cout << "Encr: " << strSubst << endl; // debug

        string strTable = "";
        i = 0;
        for (i; i < strlen(strUniq); i++)
        {
            strTable += strUniq[i];
            strTable += ':';
            strTable += strSubst[i];
            strTable += ' ';
        }
    //    cout << "Substitution table: " << strTable << endl;

        i = 0;
        for (i; i<originTextLen; i++)
        {
            encryptedText[i] = strSubst[getposition(strUniq, strlen(strUniq), originText[i])];
        }

        encryptedText[originTextLen] = '\0';

    //    cout << "Encryted  : " << encryptedText << endl;
        ui->substitutionedTextEditor->setPlainText( QString(encryptedText) );
        qstrTable = QString::fromStdString(strTable);
        ui->substitutionTableEditor->setPlainText( qstrTable );
    }


}

void MainWindow::only_russian(QString text, int punctuation)
{
    char cyrillic[] = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕËЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    char cyrillic_shuffled[] = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕËЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    shuffle_array(cyrillic_shuffled, 2);

    char latin[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char latin_shuffled[strlen(latin)];
    shuffle_array(latin_shuffled, 0);

    char latin_with_punctuation[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ \n-:;`'\"!@#$%^&*,.?/()[\\]{|}~";
    char latin_with_punctuation_shuffled[strlen(latin_with_punctuation)];
    shuffle_array(latin_with_punctuation_shuffled, 1);

    string originText = text.toStdString();
    int originTextLen = originText.length();
    char encryptedText[originTextLen];

    if (punctuation > 0) // сохраняя знаки препинания
    {
//        int i = 0;
//        for (i; i<originTextLen; )
//        {
//            if (originText[i] == '\n')
//            {
//                encryptedText[i] = originText[i];
//            }
//            else {
//                if (check_if_one_byte_letter(originText[i])) {
//                    encryptedText[i] = latin_with_punctuation_shuffled[getposition(latin, strlen(latin), originText[i])];
//                    cout << originText[i] << " : " << encryptedText[i]  << endl;
//                    i++;
//                }
//                else {
//                    char ch[3] = {originText[i], originText[i+1], '\0'};
//                    int substitution_position = getposition_ru(cyrillic, ch);
//                    encryptedText[i] = cyrillic_shuffled[substitution_position];
//                    encryptedText[i+1] = cyrillic_shuffled[substitution_position+1];
//                    cout << string(ch) << " : " <<substitution_position << " " << encryptedText[i] << encryptedText[i+1] << endl;
//                    i+=2;
//                }
//            }

//        }

        int i = 0;
        for (i; i<originTextLen; )
        {
            if (check_if_one_byte_letter(originText[i])) {
                if (check_if_punctuation(originText[i])) {
                    encryptedText[i] = originText[i];
//                    cout << i << " "<< originText[i] << " : " << encryptedText[i] << encryptedText[i+1] << endl;
                }
                else {
                    encryptedText[i] = latin_shuffled[getposition(latin, strlen(latin), originText[i])];
//                    cout << i << " "<< originText[i] << " : " << encryptedText[i] << encryptedText[i+1] << endl;
                }
                i++;
            }
            else {
                char ch[3] = {originText[i], originText[i+1], '\0'};
                int substitution_position = getposition_ru(cyrillic, ch);
                encryptedText[i] = cyrillic_shuffled[substitution_position];
                encryptedText[i+1] = cyrillic_shuffled[substitution_position+1];
                cout << i << " "<< string(ch) << " : " <<substitution_position << " " << encryptedText[i] << encryptedText[i+1] << endl;
                i+=2;
            }
        }

        encryptedText[originTextLen] = '\0';

        ui->substitutionedTextEditor->setPlainText( QString(encryptedText) );
    }
    else
    {
        int i = 0;
        for (i; i<originTextLen; )
        {
            if (check_if_one_byte_letter(originText[i])) {
                encryptedText[i] = latin_with_punctuation_shuffled[getposition(latin_with_punctuation, strlen(latin_with_punctuation), originText[i])];
                cout << originText[i] << " : " << encryptedText[i]  << endl;
                i++;
            }
            else {
                char ch[3] = {originText[i], originText[i+1], '\0'};
                int substitution_position = getposition_ru(cyrillic, ch);
                encryptedText[i] = cyrillic_shuffled[substitution_position];
                encryptedText[i+1] = cyrillic_shuffled[substitution_position+1];
                cout << string(ch) << " : " <<substitution_position << " " << encryptedText[i] << encryptedText[i+1] << endl;
                i+=2;
            }

        }
//        int i = 0;
//        for (i; i<originTextLen; )
//        {
//            if (check_if_one_byte_letter(originText[i])) {
//                encryptedText[i] = latin_with_punctuation_shuffled[getposition(latin_with_punctuation, strlen(latin_with_punctuation), originText[i])];
////                cout << i << " "<< originText[i] << " : " << encryptedText[i]  << endl;
//                i++;
//            }
//            else {
//                char ch[3] = {originText[i], originText[i+1], '\0'};
//                int substitution_position = getposition_ru(cyrillic, ch);
//                encryptedText[i] = cyrillic_shuffled[substitution_position];
//                encryptedText[i+1] = cyrillic_shuffled[substitution_position+1];
//                cout << i << " "<< string(ch) << " : " <<substitution_position << " " << encryptedText[i] << encryptedText[i+1] << endl;
//                i+=2;
//            }
//        }

        encryptedText[originTextLen] = '\0';

        ui->substitutionedTextEditor->setPlainText( QString(encryptedText) );
    }

    cout << encryptedText << endl;
}

void MainWindow::make_substitution()
{
    QString plainText = ui->originTextEditor->toPlainText();
    if (plainText != "") {
        if ((onlyEn > 0) && (onlyRu == 0))
            only_english(plainText, doNotTouchPunctuation);

        if ((onlyEn == 0) && (onlyRu > 0))
            only_russian(plainText, doNotTouchPunctuation);
        else // both languages
        {

        }
    }


}

void MainWindow::shuffle_array(char * str_shuffled, int lang)
{
    char cyrillic[] = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕËЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

    if (lang == 2)
    {
        int alphabet_nums_len = strlen(cyrillic)/2;
        int alphabet_numbers[alphabet_nums_len];
        int alphabet_numbers_shuffled[alphabet_nums_len];
        int i, j = 0;
        for (i; i<alphabet_nums_len; i++)
        {
            alphabet_numbers[i] = i;
//            cout << alphabet_numbers[i] << " ";
        }
//        cout << endl;

        i = 0; j = 0;
        for (i; i < alphabet_nums_len; i++)
        {
            if (i == 0)
            {
                alphabet_numbers_shuffled[i] = get_random_int(alphabet_nums_len-1);
                j++;
            }
            else
            {
                bool compareFlag;
                do {
                    compareFlag = false;
                    alphabet_numbers_shuffled[i] = get_random_int(alphabet_nums_len-1);

                    for (int i_inner(0); i_inner < j; i_inner++)
                    {
                        if (alphabet_numbers_shuffled[i] == alphabet_numbers_shuffled[i_inner])
                        {
                            compareFlag = true;
                            i_inner = j; // переходим в конец
                        }
                    }
                } while(compareFlag);
                j++;
            }
        }

        i=0;
        for (i; i<alphabet_nums_len; i++)
        {
            int num = alphabet_numbers_shuffled[i];
            str_shuffled[2*i] = cyrillic[2*num];
            str_shuffled[2*i+1] = cyrillic[2*num+1];
        }
        str_shuffled[strlen(cyrillic)] = '\0';

        cout << cyrillic << " " << endl;
        cout << str_shuffled << " " << endl;
    }

    char latin[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (lang == 0)
    {
        int alphabet_nums_len = strlen(latin);
        cout << "N = "<< alphabet_nums_len << endl;
        int alphabet_numbers[alphabet_nums_len];
        int alphabet_numbers_shuffled[alphabet_nums_len];
        int i, j = 0;
        for (i; i<alphabet_nums_len; i++)
        {
            alphabet_numbers[i] = i;
            cout << alphabet_numbers[i] << " ";
        }
        cout << endl;

        i = 0; j = 0;
        for (i; i < alphabet_nums_len; i++)
        {
            if (i == 0)
            {
                alphabet_numbers_shuffled[i] = get_random_int(alphabet_nums_len-1);
                j++;
            }
            else
            {
                bool compareFlag;
                do {
                    compareFlag = false;
                    alphabet_numbers_shuffled[i] = get_random_int(alphabet_nums_len-1);

                    for (int i_inner(0); i_inner < j; i_inner++)
                    {
                        if (alphabet_numbers_shuffled[i] == alphabet_numbers_shuffled[i_inner])
                        {
                            compareFlag = true;
                            i_inner = j; // переходим в конец
                        }
                    }
                } while(compareFlag);
                j++;
            }
        }

        i=0;
        for (i; i<alphabet_nums_len; i++)
        {
            int num = alphabet_numbers_shuffled[i];
            str_shuffled[i] = latin[num];
    //        cout << cyrillic_shuffled[2*i] << cyrillic_shuffled[2*i+1] << endl;
        }
        str_shuffled[strlen(latin)] = '\0';

        cout << latin << " " << endl;
        cout << str_shuffled << " " << endl;
    }

    char latin_with_punctuation[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ \n-:;`'\"!@#$%^&*,.?/()[\\]{|}~";
    if (lang == 1)
    {
        int alphabet_nums_len = strlen(latin_with_punctuation);
        cout << "N = "<< alphabet_nums_len << endl;
        int alphabet_numbers[alphabet_nums_len];
        int alphabet_numbers_shuffled[alphabet_nums_len];
        int i, j = 0;
        for (i; i<alphabet_nums_len; i++)
        {
            alphabet_numbers[i] = i;
            cout << alphabet_numbers[i] << " ";
        }
        cout << endl;

        i = 0; j = 0;
        for (i; i < alphabet_nums_len; i++)
        {
            if (i == 0)
            {
                alphabet_numbers_shuffled[i] = get_random_int(alphabet_nums_len-1);
                j++;
            }
            else
            {
                bool compareFlag;
                do {
                    compareFlag = false;
                    alphabet_numbers_shuffled[i] = get_random_int(alphabet_nums_len-1);

                    for (int i_inner(0); i_inner < j; i_inner++)
                    {
                        if (alphabet_numbers_shuffled[i] == alphabet_numbers_shuffled[i_inner])
                        {
                            compareFlag = true;
                            i_inner = j; // переходим в конец
                        }
                    }
                } while(compareFlag);
                j++;
            }
        }

        i=0;
        for (i; i<alphabet_nums_len; i++)
        {
            int num = alphabet_numbers_shuffled[i];
            str_shuffled[i] = latin_with_punctuation[num];
        }

        str_shuffled[strlen(latin_with_punctuation)] = '\0';

        cout << latin_with_punctuation << " " << endl;
        cout << str_shuffled << " " << endl;
    }
}


void MainWindow::on_clearButton_clicked()
{
    clear_fields();
}


void MainWindow::on_pushButton_2_clicked()
{
    make_substitution();
}


void MainWindow::on_checkBox_en_stateChanged(int arg1)
{
    onlyEn = arg1;
    cout << onlyEn << endl;
}


void MainWindow::on_checkBox_ru_stateChanged(int arg1)
{
    onlyRu = arg1;
    cout << onlyRu << endl;
}


void MainWindow::on_checkBox_punc_stateChanged(int arg1)
{
    doNotTouchPunctuation = arg1;
    cout << doNotTouchPunctuation << endl;
}

